import './App.css';
import Jjobs from "./components/Jobs";
import Mmessages from './components/Messages';
import Rrates from './components/Rates';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Online Jobs Service</h1>
        <p>
          <Router>
            <header class="header">
              <Link class="header__heading" to="/">
                Our jobs service
              </Link>
            </header>
            <div className="App" class="app">
              <Switch>
                <Route path="/jobs">
                    <Link to="/jobs">
                      <button class="main-button">See list jobs</button>
                    </Link>
                    <Link to="/messages">
                      <button class="main-button">See personal messages</button>
                    </Link>
                    <Link to="/rates">
                      <button class="main-button">See your rate</button>
                    </Link>
                  <Jjobs />
                  <Jjobs />
                  <Jjobs />
                </Route>
                <Route path="/messages">
                <Link to="/jobs">
                      <button class="main-button">See list jobs</button>
                    </Link>
                    <Link to="/messages">
                      <button class="main-button">See personal messages</button>
                    </Link>
                    <Link to="/rates">
                      <button class="main-button">See your rate</button>
                    </Link>
                  <Mmessages />
                  <Mmessages />
                  <Mmessages />
                </Route>
                <Route path="/rates">
                <Link to="/jobs">
                      <button class="main-button">See list jobs</button>
                    </Link>
                    <Link to="/messages">
                      <button class="main-button">See personal messages</button>
                    </Link>
                    <Link to="/rates">
                      <button class="main-button">See your rate</button>
                    </Link>
                  <Rrates />
                </Route>
                <Route path="/">
                  <main class="main">
                  <Link to="/jobs">
                      <button class="main-button">See list jobs</button>
                    </Link>
                    <Link to="/messages">
                      <button class="main-button">See personal messages</button>
                    </Link>
                    <Link to="/rates">
                      <button class="main-button">See your rate</button>
                    </Link>
                    {""}
                      <h2 class="main-heading">Now it's much easier to find a job!</h2>
                  </main>
                </Route>
              </Switch>
            </div>
            <footer class="footer">
              <div class="footer__copyright">© Studemployment</div>
            </footer>
          </Router>
        </p> 
      </header>
    </div>
  );
}

export default App;

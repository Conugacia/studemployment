import React from 'react';
import * as Api from 'typescript-fetch-api'

const api = new Api.DefaultApi()

class Rrates extends React.Component {

  constructor(props) {
    super(props);
    this.state = { rates: [] };

  }

  fetchData = date => {api.rates({ page: date }).then(response => this.setState({rates: response}))}
  componentDidMount(){
    this.fetchData(this.props.date)
  
  }
  componentDidUpdate(prevProps){
    if(this.props.date!==prevProps.date) {
      this.fetchData(this.props.date)
    }

  }

  render() {
    console.log(this.props.date)
    return <div>

        {this.state.rates.map((Rates) => <div>{Rates.name} {Rates.mark} {Rates.text} </div>) }
    
    </div>
    
  }
}

export default Rrates;
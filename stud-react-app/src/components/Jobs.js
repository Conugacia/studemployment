import React from 'react';
import * as Api from 'typescript-fetch-api'

const api = new Api.DefaultApi()

class Jjobs extends React.Component {

  constructor(props) {
    super(props);
    this.state = { jobs: [] };

  }

  fetchData = date => {api.jobs({ page: date }).then(response => this.setState({jobs: response}))}
  componentDidMount(){
    this.fetchData(this.props.date)
  
  }
  componentDidUpdate(prevProps){
    if(this.props.date!==prevProps.date) {
      this.fetchData(this.props.date)
    }

  }

  render() {
    console.log(this.props.date)
    return <div>

        {this.state.jobs.map((Jobs) => <div>{Jobs.position} {Jobs.text} {Jobs.contact} {Jobs.price}</div>)}
    
    </div>
    
  }
}

export default Jjobs;
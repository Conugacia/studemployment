import React from 'react';
import * as Api from 'typescript-fetch-api'

const api = new Api.DefaultApi()

class Mmessages extends React.Component {

  constructor(props) {
    super(props);
    this.state = { messages: [] };

  }

  fetchData = date => {api.messages({ page: date }).then(response => this.setState({messages: response}))}
  componentDidMount(){
    this.fetchData(this.props.date)
  
  }
  componentDidUpdate(prevProps){
    if(this.props.date!==prevProps.date) {
      this.fetchData(this.props.date)
    }

  }

  render() {
    console.log(this.props.date)
    return <div>

        {this.state.messages.map((Message) => <div>{Message.username}  {Message.time} {Message.text}</div>)}
    
    </div>
    
  }
}

export default Mmessages;
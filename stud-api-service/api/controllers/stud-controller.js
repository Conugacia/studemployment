'use strict';


let faker = require('faker');



module.exports = {
  jobs: getJobs,
  messages: getMessages,
  rates: getRates,
  
};


function getJobs(req, res) {
  const dolznost = [ "Ruby ", "Python", " Angular", "Java", "Java-Script", "C++", "C#","Ruby"]
  res.json([{

    "text": "" + faker.lorem.sentence(12),
    "position": "" + dolznost[Math.floor(Math.random() * dolznost.length)],
    "price": "ЗП : " + faker.random.number({
      min:500,
      max:1000}) + "$",
    "contact":" " + faker.internet.email(),},
    
    {
      "text": "" + faker.lorem.sentence(12),
    "position": "" + dolznost[Math.floor(Math.random() * dolznost.length)],
    "price": "ЗП : " + faker.random.number({
      min:500,
      max:1000}) + "$",
    "contact":" " + faker.internet.email(),},

    {
      "text": "" + faker.lorem.sentence(12),
      "position": "" + dolznost[Math.floor(Math.random() * dolznost.length)],
      "price": "ЗП : " + faker.random.number({
        min:500,
        max:1000}) + "$",
      "contact":" " + faker.internet.email(),
  }]);
}

function getMessages(req, res) {
  const sex = ["male", "feamale"]
  res.json([{
    "username": "" + faker.name.firstName() + " " + faker.name.lastName(),
    "text": "" + faker.lorem.sentence(4),
    "time": "" + faker.random.number({
      min:0,
      max:23}) + ":" + faker.random.number({
        min:0,
        max:59}),
  },
  {
    "username": "" + faker.name.firstName() + " " + faker.name.lastName(),
    "text": "" + faker.lorem.sentence(4),
    "time": "" + faker.random.number({
      min:0,
      max:23}) + ":" + faker.random.number({
        min:0,
        max:59}),
  },
  {
    "username": "" + faker.name.firstName() + " " + faker.name.lastName(),
    "text": "" + faker.lorem.sentence(4),
    "time": "" + faker.random.number({
      min:0,
      max:23}) + ":" + faker.random.number({
        min:0,
        max:59}),
  }
]);
}

function getRates(req, res) {
  res.json([{
    "name": "" + faker.lorem.words(2),
    "mark": "" + faker.random.number({
      min:1,
      max:10}),
    "text": "" + faker.lorem.sentence(6),
  }]);
}

